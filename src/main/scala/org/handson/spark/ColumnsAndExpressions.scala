package org.handson.spark

import org.apache.spark.sql.functions.{col, column, expr}
import org.apache.spark.sql.{Column, DataFrame, SparkSession}

object ColumnsAndExpressions extends App {

  if (System.getProperty("os.name").toLowerCase().contains("win")) {
    System.setProperty("hadoop.home.dir", "C:\\Users\\djurkovic\\Downloads\\chunked\\hadoop\\")
  }

  val spark = SparkSession.builder()
    .appName("DF Columns and Expressions")
    .config("spark.master", "local")
    .getOrCreate()

  val carsDF = spark.read
    .option("inferSchema", "true")
    .json("src/main/resources/data/cars.json")

  val firstColumn: Column = carsDF.col("Name")

  import spark.implicits._

  carsDF.select(
    carsDF.col("Name"),
    col("Acceleration"),
    column("Weight_in_lbs"),
    'Year, // Scala Symbol, auto-converted to column
    $"Horsepower", // fancier interpolated string, returns a Column object
    expr("Origin") // EXPRESSION
  )
  //.show(10)

  carsDF.select("Name", "Year")
  //.show(20)
  // EXPRESSIONS
  val simplestExpression: Column = carsDF.col("Weight_in_lbs")
  val weightInKgExpression: Column = simplestExpression / 2.2

  val carsWithWeightsDF: DataFrame = carsDF.select(
    col("Name"),
    col("Weight_in_lbs"),
    weightInKgExpression.as("Weight_in_kg"),
    expr("Weight_in_lbs / 0").as("Weight_in_kg_2")
  )

  val carsWithSelectExprWeightsDF = carsDF.selectExpr(
    "Name",
    "Weight_in_lbs",
    "Weight_in_lbs / 2.2"
  )

  val moviesDF = spark.read.option("inferSchema", "true").json("src/main/resources/data/movies.json")

  moviesDF
    .select("Title", "Release_Date")

  moviesDF.select(
    moviesDF.col("Title"),
    col("Release_Date"),
    col("Major_Genre"),
    $"IMDB_Rating"
  )
  //    .show(10)

  moviesDF
    .selectExpr(
      "Title",
      "US_Gross",
      "WorldWide_Gross",
      "US_Gross + WorldWide_Gross as Total_Gross"
    )
  //.show(10)

  moviesDF.select("Title", "US_Gross", "Worldwide_Gross")
    .withColumn("Total_Gross", col("US_Gross") + col("Worldwide_Gross"))
    .withColumn("Total_Gross_1", col("Total_Gross"))
  //    .show(10)

  val europeanCarsDF = carsDF.filter(col("Origin") =!= "USA")
  val europeanCarsDF2 = carsDF.where(col("Origin") =!= "USA")
  val americanCarsDF = carsDF.filter("Origin = 'USA'")

  val americanPowerfulCarsDF = carsDF.filter(col("Origin") === "USA").filter(col("Horsepower") > 150)
  val americanPowerfulCarsDF2 = carsDF.filter(col("Origin") === "USA" and col("Horsepower") > 150).show(100)
  carsDF.filter("Origin = 'USA' and Horsepower > 150").explain(true)


  val actionMovie = moviesDF.select("Title", "IMDB_Rating")
    .where(col("Major_Genre") === "Action")
    .where(col("IMDB_Rating") > 6)
    .show(100)

  spark.stop()

}
