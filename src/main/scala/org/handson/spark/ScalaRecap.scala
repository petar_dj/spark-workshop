package org.handson.spark

object ScalaRecap extends App {


  case class Person(name: String) {
    def greet = println(s"Hi, my name is $name")
  }

  implicit def fromStringToPerson(name: String) = Person(name)


  def toPerson(name: String) = {
    Person(name)
  }

  val person = toPerson("Misko").greet
  val person2 = "Misko".greet


  val anIfExpression = if(2 > 3) "bigger" else "smaller" + " hdhdh "

  println(anIfExpression)

}
