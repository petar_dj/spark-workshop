package org.handson.spark

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.types._


object DataFrames extends App {

  if (System.getProperty("os.name").toLowerCase().contains("win")) {
    System.setProperty("hadoop.home.dir", "C:\\Users\\djurkovic\\Downloads\\chunked\\hadoop\\")
  }

  val spark: SparkSession = SparkSession.builder()
    .appName("Data Sources and Formats")
    .config("spark.master", "local")
    .getOrCreate()

  val carsSchema: StructType = StructType(Array(
    StructField("Name", StringType),
    StructField("Miles_per_Gallon", DoubleType),
    StructField("Cylinders", LongType),
    StructField("Displacement", DoubleType),
    StructField("Horsepower", LongType),
    StructField("Weight_in_lbs", LongType),
    StructField("Acceleration", DoubleType),
    StructField("Year", DateType),
    StructField("Origin", StringType)
  ))

  val carsDF: DataFrame = spark.read
    .format("json")
    //.schema(carsSchema) // enforce a schema
    .option("mode", "failFast") // dropMalformed, permissive (default)
    .option("path", "src/main/resources/data/cars.json")
    .load()

  carsDF.printSchema()
  carsDF.show(10)
  //    carsDF.write.save("src/main/resources/data/cars.parquet")


}
