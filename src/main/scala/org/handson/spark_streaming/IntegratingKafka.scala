package org.handson.spark_streaming

import org.apache.spark.sql.functions.{col, expr, from_json}
import org.apache.spark.sql.{DataFrame, SparkSession}
import common._

object IntegratingKafka extends App {

  val spark = SparkSession.builder()
    .appName("Integrating Kafka")
    .master("local[2]")
    .getOrCreate()

  spark.sparkContext.setLogLevel("ERROR")

  def readFromKafka() = {

    val kafkaDF: DataFrame = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "localhost:9092")
      .option("startingOffsets", "earliest")
      .option("subscribe", "petar1")
      .load()

    kafkaDF
      .selectExpr("CAST(value AS STRING)")
      .select(from_json(col("value"), carsSchema).as("value"))
      .writeStream
      .format("console")
      .outputMode("append")
      .start()
      .awaitTermination()
  }

  readFromKafka()

}
