package org.handson.spark_streaming

import common._
import org.apache.spark.sql.functions.{col, expr, struct, to_json}
import org.apache.spark.sql.{DataFrame, SparkSession}

object WriteToKafka extends App {

  val spark = SparkSession.builder()
    .appName("Integrating Kafka")
    .master("local[1]")
    .getOrCreate()

  def writeToKafka() = {
    val carsDF = spark.readStream
      .schema(carsSchema)
      .json("src/main/resources/data/cars/")

    val carsKafkaDF = carsDF.select(col("Name").as("key"), to_json(struct("*")).as("value"))

    carsKafkaDF.writeStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "localhost:9092")
      .option("topic", "")
      .option("checkpointLocation", "./src/main/resources/data/checkpoints") // without checkpoints the writing to Kafka will fail
      .start()
      .awaitTermination()
  }

  writeToKafka()

}
