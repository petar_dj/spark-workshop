package org.handson.spark_streaming

import org.apache.spark.sql.functions._
import common._
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import org.apache.spark.sql.streaming.{StreamingQuery, Trigger}

import scala.concurrent.duration._

object StreamingDataFrames extends App {

  val spark = SparkSession.builder()
    .appName("first streams")
    .master("local[2]")
    .getOrCreate()
  spark.sparkContext.setLogLevel("ERROR")

  def readFromSocket() = {
    // reading a DF
    val lines: DataFrame = spark.readStream
      .format("socket")
      .option("host", "localhost")
      .option("port", 12345)
      .load()

    // transformation
    val shortLines: DataFrame = lines
      .filter(length(col("value")) <= 5)
      .filter(col("").contains("test"))
    // tell between a static vs a streaming DF
    println(shortLines.isStreaming)

    // consuming a DF
    val query: StreamingQuery = shortLines.writeStream
      .format("console")
      .outputMode("append")
      .start()

    // wait for the stream to finish
    query.awaitTermination()
  }

  def readFromFiles() = {
    val stocksDF: DataFrame = spark.readStream
      .format("csv")
      .option("header", "false")
      .option("dateFormat", "MMM d yyyy")
      .schema(stocksSchema)
      .load("src/main/resources/data/stocks")

    val filteredDF: Dataset[Row] = stocksDF
      .filter(col("company").contains("AAPL"))
      .filter(col("value") > 30)

    filteredDF.writeStream
      .format("console")
      .option("numRows",150)
      .outputMode("append")
      .start()
      .awaitTermination()
  }

  def demoTriggers() = {
    val lines: DataFrame = spark.readStream
      .format("socket")
      .option("host", "168.119.224.237")
      .option("port", 12345)
      .load()

    // write the lines DF at a certain trigger
    lines
      .writeStream
      .format("console")
      .outputMode("append")
      .trigger(
         Trigger.ProcessingTime(2.seconds) // every 2 seconds run the query
        // Trigger.Once() // single batch, then terminate
//        Trigger.Continuous(2.seconds) // experimental, every 2 seconds create a batch with whatever you have
      )
      .start()
      .awaitTermination()
  }

  demoTriggers()
}
