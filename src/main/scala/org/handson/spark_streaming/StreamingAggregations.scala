package org.handson.spark_streaming

import org.apache.spark.sql.functions._
import common._
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import org.apache.spark.sql.streaming.{StreamingQuery, Trigger}
import org.apache.spark.sql.functions._

import scala.concurrent.duration._

object StreamingAggregations extends App {
  val spark = SparkSession.builder()
    .appName("Aggregations")
    .master("local[2]")
    .getOrCreate()

  def streamingCount() = {
    val lines: DataFrame = spark.readStream
      .format("socket")
      .option("host", "localhost")
      .option("port", 12345)
      .load()

    val numbers = lines.select(col("value").cast("integer").as("number"))
    val aggregationDF = numbers.select(sum(col("number")).as("agg_so_far"))
    // aggregations with distinct are not supported
    // otherwise Spark will need to keep track of EVERYTHING

    aggregationDF.writeStream
      .format("console")
      .outputMode("complete") // append and update not supported on aggregations without watermark
      .start()
      .awaitTermination()
  }
}
